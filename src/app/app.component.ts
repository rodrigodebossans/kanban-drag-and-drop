import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  dropzones: NodeListOf<HTMLElement>;
  cards: NodeListOf<HTMLElement>;

  constructor() {}

  ngOnInit(): void {
    this.initHandleZones();
    this.initHandleCards();
  }

  /**
   * Adiciona handlers drag and drop para os zones
   */

  initHandleZones() {
    this.dropzones = document.querySelectorAll('.dad-dropzone');
    this.dropzones.forEach((dropzone) => {
      dropzone.addEventListener('dragenter', this.onDragEnter);
      dropzone.addEventListener('dragover', this.onDragOver.bind(this));
      dropzone.addEventListener('dragleave', this.onDragLeave);
      dropzone.addEventListener('drop', this.onDrop.bind(this));
    });
  }

  /**
   * Adiciona handlers drag and drop para os cards
   */

  initHandleCards() {
    this.cards = document.querySelectorAll('.dad-card');
    this.cards.forEach((card) => {
      card.addEventListener('dragstart', this.onDragStart.bind(this));
      card.addEventListener('dragend', this.onDragEnd.bind(this));
    });
  }

  /**
   * Acionado quando o usuário começa a arrastar um elemento válido
   * ou seleção de texto.
   */

  onDragStart(eventCard: DragEvent | any): void {
    // console.log('dragStart =>', eventCard);
    eventCard.target.classList.add('is-dragging');
    this.dropzones.forEach((dropzone: HTMLElement) => {
      dropzone.classList.add('highlight');
    });
    eventCard.dropEffect = 'move';
    eventCard.dataTransfer.setData(
      'elementIdentification',
      eventCard.target.id
    );
  }

  /**
   * Permitindo drop
   */

  allowDrop(eventCard: DragEvent | any) {
    if (eventCard.target.getAttribute('droppable') == 'false') {
      eventCard.dataTransfer.dropEffect = 'none'; // dropping is not allowed
      eventCard.preventDefault();
    } else {
      eventCard.dataTransfer.dropEffect = 'all'; // drop it like it's hot
      eventCard.preventDefault();
    }
  }

  /**
   * Acionado quando uma operação de arrastar está terminando
   * (por exmplo, ao soltar o botão do mouse ou pressionar a tecla esc)
   */

  onDragEnd(eventCard: DragEvent | any): void {
    // console.log('dragEnd =>', eventCard);
    eventCard.target.classList.remove('is-dragging');
    this.dropzones.forEach((dropzone: HTMLElement) => {
      dropzone.classList.remove('highlight');
    });
  }

  /**
   * Acionado quando um elemento arrastável ou seleção de texto entra em um
   * ponto de soltura (drop target)
   */

  onDragEnter(eventCard: DragEvent | any): void {
    // console.log('dragEnter =>', eventCard);
  }

  /**
   * Acionado quando um elemento ou seleção de texto está sendo arrastado sobre
   * um ponto de soltura válido (a cada aproximadamente 100 milisegundos).
   */

  onDragOver(eventCard: DragEvent | any): void {
    // console.log('dragOver =>', eventCard);
    this.allowDrop(eventCard);
  }

  /**
   * Acionado quando um elemento arrastável ou seleção de texto abandona um ponto
   * de soltura (drop target) válido.
   */

  onDragLeave(eventCard: DragEvent | any): void {
    // console.log('dragLeave =>', eventCard);
  }

  /**
   * Acionado quando um elemento ou seleção de texto é solta em um ponto de soltura
   * (drop target) válido.
   */

  onDrop(eventCard: DragEvent | any): void {
    // console.log('drop =>', eventCard);
    const cardBeingDragged = document.getElementById(
      eventCard.dataTransfer.getData('elementIdentification')
    );
    this.deductCorrectDropZone(eventCard).appendChild(cardBeingDragged);
  }
  
  deductCorrectDropZone(eventCard: DragEvent | any): HTMLElement {
    let correctDropZone;
    if (!eventCard.target.classList.contains('dad-dropzone')) {
      correctDropZone = eventCard.path.filter(
        (element) =>
          element.classList && element.classList.contains('dad-dropzone')
      )[0];
    } else {
      correctDropZone = eventCard.target;
    }
    return correctDropZone;
  }

}
